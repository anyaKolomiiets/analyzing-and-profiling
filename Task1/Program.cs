﻿using System.Security.Cryptography;

string GeneratePasswordHashUsingSalt(string passwordText, byte[] salt)
{
    var iterate = 10000;
    var pbkdf2 = new Rfc2898DeriveBytes(passwordText, salt, iterate);
    byte[] hash = pbkdf2.GetBytes(20);

    byte[] hashBytes = new byte[36];
    Array.Copy(salt, 0, hashBytes, 0, 16);
    Array.Copy(hash, 0, hashBytes, 16, 20);

    var passwordHash = Convert.ToBase64String(hashBytes);

    return passwordHash;
}

GeneratePasswordHashUsingSalt("test7696969696", new byte[] { 1, 2, 3, 5, 6, 7, 3, 7, 45, 6, 4, 3, 4, 5, 2, 4, 6, 8 });